import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { ActivatedRoute } from '@angular/router';
import { Product} from 'src/app/interfaces/product';


@Component({
  selector: 'app-show-product',
  templateUrl: './show-product.component.html',
  styleUrls: ['./show-product.component.css']
})
export class ShowProductComponent implements OnInit {
  product: Product;

  constructor(private service: ProductService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.service.getProductById(this.route.snapshot.params.id).subscribe(data => {
      this.product = data;
    })
  }

}
