import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';
import { ProductService } from 'src/app/services/product.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './allclient-product.component.html',
  styleUrls: ['./allclient-product.component.css']
})
export class AllClientProductComponent implements OnInit {

  products: any[];

  constructor(private service: ProductService,
    private route: ActivatedRoute,  private router: Router ) { }

  ngOnInit() {
      this.service.getAllProducts().subscribe(data => {
        this.products = data;
      })

  }
  showProduct(id: number){
    this.router.navigate(["/show-product/"+id]);
  }
}

