import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllClientProductComponent } from './allclient-product.component';

describe('AllClientProductComponent', () => {
  let component: AllClientProductComponent;
  let fixture: ComponentFixture<AllClientProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllClientProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllClientProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
