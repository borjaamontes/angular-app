import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.css']
})
export class NewProductComponent implements OnInit {

  addProductForm: FormGroup;
  showError: boolean = false;

  constructor(private service: ProductService, private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.addProductForm = this.fb.group({
      id:[Math.floor(Math.random()*1000)],
      name:[null, Validators.required],
      sku:[null, Validators.required],
      description:[null, Validators.compose([Validators.required, Validators.minLength(10)])],
      size:"Única",
      color:"",
      price:0,
      isAvailable:true,
      image: ""
    })
  }

  onSubmit(){
    this.service.addProduct(this.addProductForm.value).subscribe(data => {
      this.router.navigate(["/products"]);
    }, error => {
      this.showError = true;
    })
  }

}
