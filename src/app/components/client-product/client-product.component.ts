import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';
import { ProductService } from 'src/app/services/product.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './client-product.component.html',
  styleUrls: ['./client-product.component.css']
})
export class ClientProductComponent implements OnInit {

  products: any[];

  constructor(private service: ProductService,
    private route: ActivatedRoute,  private router: Router ) { }

  ngOnInit() {

    this.route.queryParams.subscribe(
      qparams=>{
        let q = qparams['q'];
        this.service.getProductByName(q)
          .subscribe(data=>this.products=data)
      }
    )
    ;
  }
  showClientProduct(id: number){
    this.router.navigate(["/show-client-product/"+id]);
  }
}

