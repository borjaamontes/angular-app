import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {

  product: any;
  updateProductForm: FormGroup;

  constructor(private service: ProductService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.service.getProductById(this.route.snapshot.params.id).subscribe(data => {
      this.product = data;

      this.updateProductForm = this.fb.group({
        id:[data.id],
        name:[data.name, Validators.required],
        description:[data.description, Validators.compose([Validators.required, Validators.minLength(10)])],
        size:[data.size],
        color:[data.color],
        sku:[data.sku],
        price:[data.price],
        isAvailable:true,
      })

    })
  }

  formatDate(date: Date){
    if(date){
      return new Date(date).toISOString().substring(0,10);
    }
  }

  onSubmit(){
    this.service.updateProduct(this.updateProductForm.value).subscribe(data => {
      this.router.navigate(["/products"]);
    })
  }

}
