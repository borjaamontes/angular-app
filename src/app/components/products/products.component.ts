import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { Router } from '@angular/router';
import { Product} from 'src/app/interfaces/product';
//import {Producto} from 'src/app/producto';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public products: Product[];

  constructor(private service: ProductService, private router: Router ) { }

  ngOnInit() {
    this.service.getAllProducts().subscribe(data => {
      this.products = data;
    })
  }

  showProduct(id: number){
    this.router.navigate(["/show-product/"+id]);
  }

  updateProduct(id: number){
    this.router.navigate(["/update-product/"+id]);
  }

  deleteProduct(id: number){
    this.router.navigate(["/delete-product/"+id]);
  }

}
