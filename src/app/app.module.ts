import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from 'src/app/components/home/home.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { NewProductComponent } from './components/new-product/new-product.component';
import { ProductsComponent } from './components/products/products.component';
import { ShowProductComponent } from './components/show-product/show-product.component';
import { UpdateProductComponent } from './components/update-product/update-product.component';
import { DeleteProductComponent } from './components/delete-product/delete-product.component';
import { RouterModule } from '@angular/router';
import { ClientProductComponent } from './components/client-product/client-product.component';
import { ProductService } from './services/product.service';
import { HeaderComponent } from './components/header/header.component';
import { ShowProductClientComponent } from './components/show-product-client/show-product-client.component';
import { AllClientProductComponent } from './components/allclient-product/allclient-product.component';
import { HeaderListComponent } from './components/header-list/header-list.component';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    NewProductComponent,
    ProductsComponent,
    ShowProductComponent,
    UpdateProductComponent,
    ClientProductComponent,
    UpdateProductComponent,
    DeleteProductComponent,
    HeaderComponent,
    ShowProductClientComponent,
    AllClientProductComponent,
    HeaderListComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'products', component: ProductsComponent},
      { path: 'new-product', component: NewProductComponent},
      { path: 'update-product/:id', component: UpdateProductComponent},
      { path: 'delete-product/:id', component: DeleteProductComponent},
      { path: 'show-product/:id', component: ShowProductComponent},
      { path: 'client-product', component: ClientProductComponent},
      { path: 'allclient-product', component: AllClientProductComponent},
      { path: 'show-client-product/:id' , component: ShowProductClientComponent}
    ])
  ],
  providers: [ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
