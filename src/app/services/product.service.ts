import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product} from 'src/app/interfaces/product';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  _baseURL: string = "https://localhost:5001/product";

  constructor(private http: HttpClient) { }

  getAllProducts(){
    return this.http.get<Product[]>(this._baseURL+"/GetProducts");
  }

  addProduct(product: Product){
    return this.http.post(this._baseURL, product);
  }

  getProductById(id: number){
    return this.http.get<Product>(this._baseURL+"/"+id);
  }

  getProductByName(name: string){
    return this.http.get<Product[]>(this._baseURL+"/name/"+name);
  }

  updateProduct(product: Product){
    return this.http.put(this._baseURL+"/"+product.id, product);
  }

  deleteProduct(id: number){
    return this.http.delete(this._baseURL+"/"+id);
  }

}
