export interface Product {
  id: number;
  sku: string;
  name: string;
  description: string;
  color: string;
  size: string;
  isAvailable: boolean;
  price: number;
  productTypeId: number;
  image: string;
}
